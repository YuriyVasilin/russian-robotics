<?php

use Bitrix\Main\Page\Asset;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
?>
<html>
<head>
<?php
Asset::getInstance()->addCss("https://fonts.googleapis.com/css?family=Josefin+Sans:300, 400,700");
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/css/bootstrap.css");
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/css/animate.css");
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/css/owl.carousel.min.css");
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/fonts/ionicons/css/ionicons.min.css");
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/fonts/fontawesome/css/font-awesome.min.css");
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/fonts/flaticon/font/flaticon.css");
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/css/style.css");

Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/js/jquery-3.2.1.min.js");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/js/jquery-migrate-3.0.0.js");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/js/popper.min.js");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/js/bootstrap.min.js");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/js/owl.carousel.min.js");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/js/jquery.waypoints.min.js");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/js/jquery.stellar.min.js");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/js/main.js");

Asset::getInstance()->addString('<meta http-equiv="Content-Type" content="text/html; charset='.LANG_CHARSET.'">'."\n");

$APPLICATION->ShowMeta("robots");
$APPLICATION->ShowMeta("keywords");
$APPLICATION->ShowMeta("description");
$APPLICATION->ShowLink("canonical");
$APPLICATION->ShowCSS();
$APPLICATION->ShowHeadStrings();
?>
    <title><?$APPLICATION->ShowTitle()?></title>
</head>

<body>

<?$APPLICATION->ShowPanel()?>

<!-- header -->
<header role="banner">
    <div class="top-bar">
        <div class="container">
            <div class="row">
                <div class="col-9 social">
                    <a href="#"><span class="fa fa-twitter"></span></a>
                    <a href="#"><span class="fa fa-facebook"></span></a>
                    <a href="#"><span class="fa fa-instagram"></span></a>
                    <a href="#"><span class="fa fa-youtube-play"></span></a>
                    <a href="#"><span class="fa fa-vimeo"></span></a>
                    <a href="#"><span class="fa fa-snapchat"></span></a>
                </div>
                <div class="col-3 search-top">
                    <!-- <a href="#"><span class="fa fa-search"></span></a> -->
                    <form action="#" class="search-top-form">
                        <span class="icon fa fa-search"></span>
                        <input type="text" id="s" placeholder="Type keyword to search...">
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="container logo-wrap">
        <div class="row pt-5">
            <div class="col-12 text-center">
                <a class="absolute-toggle d-block d-md-none" data-toggle="collapse" href="#navbarMenu" role="button" aria-expanded="false" aria-controls="navbarMenu"><span class="burger-lines"></span></a>
                <h1 class="site-logo"><a href="/">Balita</a></h1>
            </div>
        </div>
    </div>

    <nav class="navbar navbar-expand-md  navbar-light bg-light">
        <div class="container">
            <div class="collapse navbar-collapse" id="navbarMenu">
                <ul class="navbar-nav mx-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="/">Home</a>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="category.html" id="dropdown05" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Categories</a>
                        <div class="dropdown-menu" aria-labelledby="dropdown05">
                            <a class="dropdown-item" href="category.html">Lifestyle</a>
                            <a class="dropdown-item" href="category.html">Food</a>
                            <a class="dropdown-item" href="category.html">Adventure</a>
                            <a class="dropdown-item" href="category.html">Travel</a>
                            <a class="dropdown-item" href="category.html">Business</a>
                        </div>

                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/about.php">About</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="/contact/">Contact</a>
                    </li>
                </ul>

            </div>
        </div
    </nav>
</header>
<!-- END header -->

<? if ($APPLICATION->GetCurPage(false) === '/'): ?>
<section class="site-section pt-5">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <div class="owl-carousel owl-theme home-slider">
                    <div>
                        <a href="blog-single.html" class="a-block d-flex align-items-center height-lg" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/images/img_1.jpg'); ">
                            <div class="text half-to-full">
                                <div class="post-meta">
                                    <span class="category">Lifestyle</span>
                                    <span class="mr-2">March 15, 2018 </span> &bullet;
                                    <span class="ml-2"><span class="fa fa-comments"></span> 3</span>
                                </div>
                                <h3>There’s a Cool New Way for Men to Wear Socks and Sandals</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem nobis, ut dicta eaque ipsa laudantium!</p>
                            </div>
                        </a>
                    </div>
                    <div>
                        <a href="blog-single.html" class="a-block d-flex align-items-center height-lg" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/images/img_2.jpg'); ">
                            <div class="text half-to-full">
                                <div class="post-meta">
                                    <span class="category">Lifestyle</span>
                                    <span class="mr-2">March 15, 2018 </span> &bullet;
                                    <span class="ml-2"><span class="fa fa-comments"></span> 3</span>
                                </div>
                                <h3>There’s a Cool New Way for Men to Wear Socks and Sandals</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem nobis, ut dicta eaque ipsa laudantium!</p>
                            </div>
                        </a>
                    </div>
                    <div>
                        <a href="blog-single.html" class="a-block d-flex align-items-center height-lg" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/images/img_3.jpg'); ">
                            <div class="text half-to-full">
                                <div class="post-meta">
                                    <span class="category">Lifestyle</span>
                                    <span class="mr-2">March 15, 2018 </span> &bullet;
                                    <span class="ml-2"><span class="fa fa-comments"></span> 3</span>
                                </div>
                                <h3>There’s a Cool New Way for Men to Wear Socks and Sandals</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem nobis, ut dicta eaque ipsa laudantium!</p>
                            </div>
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
<!-- END section -->
<? endif; ?>

<section class="site-section">
    <div class="container">
        <div class="row mb-4">
            <div class="col-md-6">
                <h1><?$APPLICATION->ShowTitle()?></h1>
            </div>
        </div>
        <div class="row blog-entries">
            <div class="col-md-12 col-lg-8 main-content">