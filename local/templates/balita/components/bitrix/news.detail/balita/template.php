<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/** @var array $arResult */

$this->setFrameMode(true);

$section = $arResult["SECTION"]["PATH"][0]["CODE"];
?>


<div class="post-meta">
    <span class="category"><?=$section?></span>
    <span class="mr-2"><?=$arResult["DISPLAY_ACTIVE_FROM"]?></span> &bullet;
    <span class="ml-2"><span class="fa fa-comments"></span> 3</span>
</div>
<div class="post-content-body">
    <?if($arResult["DETAIL_TEXT"] <> ''):?>
        <?echo $arResult["DETAIL_TEXT"];?>
    <?else:?>
        <?echo $arResult["PREVIEW_TEXT"];?>
    <?endif?>
    <div class="row mb-5">
        <div class="col-md-12 mb-4 element-animate">
            <img src="<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>" alt="Image placeholder" class="img-fluid">
        </div>
    </div>
</div>


<div class="pt-5">
    <p>Categories:  <a href="#"><?=$section?></a></p>
</div>