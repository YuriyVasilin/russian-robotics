<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * @var array $arResult
 */
?>
<?if(!empty($arResult["ERROR_MESSAGE"]))
{
	foreach($arResult["ERROR_MESSAGE"] as $v)
		ShowError($v);
}
if($arResult["OK_MESSAGE"] <> '')
{
	?><div class="mf-ok-text"><?=$arResult["OK_MESSAGE"]?></div><?
}
?>



    <form action="<?=POST_FORM_ACTION_URI?>" method="post">
        <?=bitrix_sessid_post()?>
        <div class="row">
            <div class="col-md-4 form-group">
                <label for="name"><?=GetMessage("MFT_NAME")?></label>
                <input type="text" id="name" class="form-control ">
            </div>
            <div class="col-md-4 form-group">
                <label for="phone">Phone</label>
                <input type="text" id="phone" class="form-control ">
            </div>
            <div class="col-md-4 form-group">
                <label for="email"><?=GetMessage("MFT_EMAIL")?></label>
                <input type="email" id="email" class="form-control ">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 form-group">
                <label for="message"><?=GetMessage("MFT_MESSAGE")?></label>
                <textarea name="message" id="message" class="form-control " cols="30" rows="8"></textarea>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 form-group">
                <input type="submit" value="<?=GetMessage("MFT_SUBMIT")?>" class="btn btn-primary">
            </div>
        </div>
    </form>