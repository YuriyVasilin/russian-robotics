<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
    <div class="sidebar-box">
        <h3 class="heading">Categories</h3>
        <ul class="categories">
            <? foreach($arResult as $arItem): ?>
            <li><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?> <span><?=$arItem["ELEMENT_CNT"]?></span></a></li>
            <?endforeach?>
        </ul>
    </div>
<?endif?>
