<?php
/** @var array $arResult */
/** @var array $arParams */
/** @var array $APPLICATION */

$id = [];
foreach($arResult["ITEMS"] as $arItem) {
    $id[] = $arItem["IBLOCK_SECTION_ID"];
}

$arSection = CIBlockSection::GetList(
    ["SORT" => "ASC"],
    [
        "IBLOCK_ID" => $arItem["IBLOCK_ID"],
        "ID" => $id
    ],
);

$arResult["SECTIONS"] = [];
while ($section = $arSection->GetNext()) {
    $arResult["SECTIONS"][intval($section["ID"])] = $section["NAME"];
}

if ($arParams["PARENT_SECTION_CODE"]) {
    $APPLICATION->SetPageProperty("title", $arParams["PARENT_SECTION_CODE"]." Category");
}